#!/usr/bin/python3

import sys
import pygame
from pygame.locals import *

FPS = 24
SCREENWIDTH = 800
SCREENHEIGHT = 600
MOVEX=4
MOVEJUMP=5
MOVEFALL=0.3

BLACK=(0,0,0)
WHITE=(255,255,255)

def doText (position, fgColour, bgColour, text, font, surface):
  txtsurf = font.render(text, True, fgColour, bgColour)
  txtrect = txtsurf.get_rect()
  if position [0] != None:
    txtrect.topleft = ((position [0], position [1]))
  else:
    txtrect.midtop = ((SCREENWIDTH / 2 ,position [1]))
  txt = surface.blit(txtsurf, txtrect)
  return txt

class Me(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load('player.png').convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.fally = y
        self.rect.y = y
        self.movex=0
        self.movey=0
        self.jump=False
        self.dead=False
        self.win=False
    def update(self):
        self.rect.x+=self.movex
        self.fally+=self.movey
        self.rect.y=self.fally
        if self.rect.left<0:
            self.rect.left=0
        if self.rect.right>SCREENWIDTH-1:
            self.rect.right=SCREENWIDTH-1

class Win(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load('win.png').convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

class Enemy(pygame.sprite.Sprite):
    def __init__(self, x, y, minx, maxx, dir):
        super().__init__()
        self.image = pygame.image.load('enemy.png').convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.minx=minx
        self.maxx=maxx
        self.dir=dir
    def update(self):
        self.rect.x+=self.dir*MOVEX
        if self.rect.right>=self.maxx:
            self.dir=-self.dir
            self.rect.right=self.maxx
        elif self.rect.left<=self.minx:
            self.dir=-self.dir
            self.rect.left=self.minx

class Platform(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load('platform.png').convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

class Level(object):
    def __init__(self):
        super().__init__()
        self.platform_list=pygame.sprite.Group()
        self.enemy_list=pygame.sprite.Group()
        self.player=pygame.sprite.Sprite()
        self.win=pygame.sprite.Sprite()
    def update(self):
        self.enemy_list.update()
        me=self.player
        org=me.rect.y
        me.rect.y+=3
        collide=pygame.sprite.spritecollideany(me, self.platform_list)
        me.rect.y=org
        if not collide:
            me.movey+=MOVEFALL
            me.jump=False
        else:
            me.rect.bottom=collide.rect.top
            me.movey=0
            if me.jump:
                me.movey=-MOVEJUMP
                me.jump=False
        self.player.update()
        if pygame.sprite.spritecollideany(self.player,self.enemy_list):
            me.dead=True
        if pygame.sprite.collide_rect(self.player,self.win):
            me.win=True
    def draw(self,SCREEN):
        self.platform_list.draw(SCREEN)
        #for r in self.platform_list:
        #    pygame.draw.rect(SCREEN, (0,0,255), r.rect, 1)
        SCREEN.blit(self.win.image,self.win.rect)
        #pygame.draw.rect(SCREEN, (0,0,255), self.win.rect, 1)
        self.enemy_list.draw(SCREEN)
        #for e in self.enemy_list:
        #    pygame.draw.rect(SCREEN, (0,0,255), e.rect, 1)
        SCREEN.blit(self.player.image,self.player.rect)
        #pygame.draw.rect(SCREEN, (0,0,255), self.player.rect, 1)

def game ():
    SCREEN = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT), 0)
    FPSCLOCK = pygame.time.Clock()
    levels=2
    thisLevel=1
    lives=3
    while thisLevel<=levels and lives > 0:
        level=Level()
        running=True
        if thisLevel==1:
            for x in range(0,18):
                level.platform_list.add(Platform(50+x*32,500))
            level.enemy_list.add(Enemy(100,500-32,100,400,-1))
            level.enemy_list.add(Enemy(400,500-32,400,700,-1))
            me=Me(550,300-32)
            win=Win(50+0*32,500-32)
        elif thisLevel==2:
            for x in range(0,18):
                level.platform_list.add(Platform(50+x*32,500))
            level.enemy_list.add(Enemy(100,500-32,100,400,-1))
            level.enemy_list.add(Enemy(400,500-32,400,700,-1))
            me=Me(50+0*32,300-32)
            win=Win(550,500-32)
        level.player=me
        level.win=win
        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    sys.exit()

            keys=pygame.key.get_pressed()
            me.movex=0
            if keys[pygame.K_LEFT]:
                me.movex=-MOVEX
            elif keys[pygame.K_RIGHT]:
                me.movex=MOVEX
            if keys[pygame.K_UP]:
                me.jump=True

            level.update()

            SCREEN.fill(BLACK)
            level.draw(SCREEN)

            doText ((0,0), WHITE, BLACK, "Level: " + str(thisLevel), FONT32, SCREEN)
            doText ((400,0), WHITE, BLACK, "Lives: " + str(lives), FONT32, SCREEN)

            if me.dead==True:
                running=False
                lives-=1

            if me.win==True:
                thisLevel+=1
                running=False

            pygame.display.flip()

            FPSCLOCK.tick (FPS)

    if lives > 0:
        print ("You win")
    else:
        print ("You lose")

if __name__ == "__main__":
    pygame.init()
    try:
        #FONT16 = pygame.font.Font('freesansbold.ttf', 16)
        FONT32 = pygame.font.Font('freesansbold.ttf', 32)
    except OSError:
        print ("Can't find font (freesansbold.ttf)...")
        sys.exit(1)
    game()
    pygame.quit()
    sys.exit()
else:
    sys.exit()
